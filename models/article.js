const mongoose = require ('mongoose');

//Article Schema

const articleSchema = mongoose.Schema({
  title: {
    type: String,
    required: true
  },
  author: {
    type: String,
    require: true
  },
  body: {
    type: String,
    require: true
  }
});

const Article =  mongoose.model('Article', articleSchema);

module.exports = Article;
