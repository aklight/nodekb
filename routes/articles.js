const express = require('express');
const router = express.Router();

// Bring in Article model

const Article = require('../models/article.js');

// User models

const User = require('../models/user.js');

const ensureAuthenticated = (req, res, next) => {
  if (req.isAuthenticated()) {
    return next();
  } else {
    req.flash('danger', 'Please login');
    res.redirect('/users/login');
  }
};

//Load Edit Form

router.get('/edit/:id', ensureAuthenticated, (req, res) => {
  Article.findById(req.params.id, (err, article) => {
    if (article.author !== req.user._id) {
      req.flash('danger', 'Not authorized');
      res.redirect('/');
    }
    res.render('edit_article', {
      title: 'Edit Article',
      article
    });
  });
});

// Add Route

router.get('/add', ensureAuthenticated, (req, res) => {
  res.render('add_article', {
    title: 'Add article'
  });
});

// Add Submit POST Route

router.post('/add', (req, res) => {
  req.checkBody('title', 'Title is required!').notEmpty();
  //req.checkBody('author', 'Author is required!').notEmpty();
  req.checkBody('body', 'Body is required!').notEmpty();

  // Get errors

  let errors = req.validationErrors();

  if (errors) {
    res.render('add_article', {
      title: 'Add article',
      errors
    });
  } else {

    const {
      title,
      body
    } = req.body;

    let author = req.user._id;

    let article = new Article({
      title,
      author,
      body
    });

    article.save((err) => {
      if (err) {
        console.log(err);
        return;
      } else {
        req.flash('success', 'Article added');
        res.redirect('/');
      }
    });
  }
});

// Edit Submit POST Route

router.post('/edit/:id', (req, res) => {
  let article = {};

  const {
    title,
    author,
    body
  } = req.body;

  article.title = title;
  article.author = author;
  article.body = body;

  let query = {
    _id: req.params.id
  };

  Article.update(query, article, (err) => {
    if (err) {
      console.log(err);
      return;
    } else {
      req.flash('success', 'Article updated');
      res.redirect('/');
    }
  });
});

// Deleteing Article

router.delete('/:id', (req, res) => {
  if (req.user._id) {
    res.status(500).send();
  }

  let query = {
    _id: req.params.id
  };

  Article.findById(req.params.id, (err, article) => {
    if (err) {
      console.log(err);
    }

    if (article.author !== req.user._id) {
      res.status(500).send();
    } else {
      Article.remove(query, (err) => {
        if (err) {
          console.log(err);
          return;
        } else {
          res.send('Success');
        }
      });
    }
  });
});

//Get single Article

router.get('/:id', (req, res) => {
  Article.findById(req.params.id, (err, article) => {
    User.findById(article.author, (err, user) => {
      res.render('article', {
        article,
        author: user.name
      });
    })
  });
});

module.exports = router;
